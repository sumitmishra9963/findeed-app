package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectLanguage extends AppCompatActivity {

    TextView txtenglish, txthindi, txtpunjabi, circleenglish, circlehindi, circlepunjabi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        txtenglish = findViewById(R.id.txt_english);
        txthindi  = findViewById(R.id.hindi);
        txtpunjabi = findViewById(R.id.punjabi);

        circleenglish = findViewById(R.id.circle_english);
        circlehindi = findViewById(R.id.circle_hindi);
        circlepunjabi = findViewById(R.id.circle_punjabi);

        txtenglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
        txthindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
        txtpunjabi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });


        circleenglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
        circlehindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
        circlepunjabi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
