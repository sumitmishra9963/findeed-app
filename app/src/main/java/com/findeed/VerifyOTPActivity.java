package com.findeed;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.Otp.OtpResponse;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class VerifyOTPActivity extends AppCompatActivity {

    TextView textcount, editnumber;
    EditText edtone, edttwo, edtthree, edtfour;
    Button btnstart;
    String otp ,phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);


        Intent i =getIntent();
        phoneNumber=i.getStringExtra("phone");
        textcount = findViewById(R.id.counttime);
        editnumber = findViewById(R.id.edit_number);
        edtone = findViewById(R.id.edt1);
        edttwo = findViewById(R.id.edt2);
        edtthree = findViewById(R.id.edt3);
        edtfour = findViewById(R.id.edt4);
        btnstart = findViewById(R.id.btnstart);

         edtone.addTextChangedListener(otpTextWatcher);
         edttwo.addTextChangedListener(otpTextWatcher);
         edtthree.addTextChangedListener(otpTextWatcher);
         edtfour.addTextChangedListener(otpTextWatcher);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                textcount.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                textcount.setText("Resend OTP");
            }
        }.start();


        editnumber.setOnClickListener(v -> {

            otp=edtone.getText().toString()+edttwo.getText().toString()+edtthree.getText().toString()+edtfour.getText().toString();



//            Intent intent1 = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
//            startActivity(intent1);
        });

        btnstart.setOnClickListener(v -> {
            new verifyOTP().execute();

        });

        edtone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Integer textlength1 = edtone.getText().length();

                if (textlength1 >= 1) {
                    edttwo.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Integer textlength2 = edttwo.getText().length();

                if (textlength2 >= 1) {
                    edtthree.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Integer textlength3 = edttwo.getText().length();

                if (textlength3 >= 1) {
                    edtfour.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private TextWatcher otpTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String editOneInput = edtone.getText().toString().trim();
            String editTwoInput = edttwo.getText().toString().trim();
            String editThirdInput = edtthree.getText().toString().trim();
            String editFourInput = edtfour.getText().toString().trim();
            if(!editFourInput.isEmpty()){
                hideKeyboard();
            }
            btnstart.setEnabled(!editOneInput.isEmpty() && !editTwoInput.isEmpty() && !editThirdInput.isEmpty() && !editFourInput.isEmpty());



        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private   void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public class verifyOTP extends AsyncTask<String,String,String>{

        @Override
        protected void onPostExecute(String s) {

            if(s.equals("Success")){
                Intent intentwelcome = new Intent(VerifyOTPActivity.this, WelcomeActivity.class);
                startActivity(intentwelcome);
                Toast.makeText(getApplicationContext(),"OTP Matched",Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(),"Invalid OTP",Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            RequestBody reqbody = RequestBody.create(null, new byte[0]);
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/otp/validateOTP/"+phoneNumber+"/"+otp)
                    .post(reqbody)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Gson gson = new Gson();
                OtpResponse check = gson.fromJson(resData,OtpResponse.class);

               return  check.getMessage();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
}



