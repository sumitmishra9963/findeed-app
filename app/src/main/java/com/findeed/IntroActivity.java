package com.findeed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPageAdapter introViewPageAdapter ;
    int position = 0;
    Animation btnAnim;
    TextView txtnext, txtskip, txtGetStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(restorePrefData())
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_intro);
        txtnext = findViewById(R.id.txt_next);
        txtskip = findViewById(R.id.txt_skip);
        txtGetStarted = findViewById(R.id.txt_getstarted);



        final List<ScreenItem> mList = new ArrayList<>();
        mList.add(new ScreenItem("Give","Provided By Findeed", R.drawable.image1));
        mList.add(new ScreenItem("Help","Provided By Findeed", R.drawable.image2));
        mList.add(new ScreenItem("Take","Provided By Findeed", R.drawable.image3));

        screenPager = findViewById(R.id.screen_viewpager);
        introViewPageAdapter = new IntroViewPageAdapter(this, mList);
        screenPager.setAdapter(introViewPageAdapter);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_animation);

        txtskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });



        txtnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                position = screenPager.getCurrentItem();
                if(position < mList.size())
                {
                      position++;
                      screenPager.setCurrentItem(position);
                }

                if (position==mList.size()-1)
                    loadLastScreen();

            }
        });

        txtGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
                startActivity(intent);

                savePrefsData();
            }
        });
    }

    private boolean restorePrefData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myprefs", MODE_PRIVATE);
        Boolean isIntroActivityOpenedBefore = pref.getBoolean("isIntroOpened", false);
        return isIntroActivityOpenedBefore;
    }

    private void savePrefsData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myprefs", MODE_PRIVATE);
        SharedPreferences.Editor editor =   pref.edit();
        editor.putBoolean("isIntroOpend", true);
        editor.commit();
    }

    private void loadLastScreen() {

        txtnext.setVisibility(View.INVISIBLE);
        txtGetStarted.setVisibility(View.VISIBLE);
        txtskip.setVisibility(View.INVISIBLE);

        txtGetStarted.setAnimation(btnAnim);
       // finish();
    }
}
