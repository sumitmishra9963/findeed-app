package com.findeed.KYC.VideoIdKYCREq;

public class headers {
    private String client_code;
    private String sub_client_code;
    private String actor_type;
    private String channel_code;
    private String stan;
    private String user_handle_type;
    private String user_handle_value;
    private String transmission_datetime;
    private String run_mode;
    private String operation_mode;
    private String channel_version;
    private String function_code;
    private String function_sub_code;

    public String getClient_code() {
        return client_code;
    }

    public void setClient_code(String client_code) {
        this.client_code = client_code;
    }

    public String getSub_client_code() {
        return sub_client_code;
    }

    public void setSub_client_code(String sub_client_code) {
        this.sub_client_code = sub_client_code;
    }

    public String getActor_type() {
        return actor_type;
    }

    public void setActor_type(String actor_type) {
        this.actor_type = actor_type;
    }

    public String getChannel_code() {
        return channel_code;
    }

    public void setChannel_code(String channel_code) {
        this.channel_code = channel_code;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getUser_handle_type() {
        return user_handle_type;
    }

    public void setUser_handle_type(String user_handle_type) {
        this.user_handle_type = user_handle_type;
    }

    public String getUser_handle_value() {
        return user_handle_value;
    }

    public void setUser_handle_value(String user_handle_value) {
        this.user_handle_value = user_handle_value;
    }


    public String getTransmission_datetime() {
        return transmission_datetime;
    }

    public void setTransmission_datetime(String transmission_datetime) {
        this.transmission_datetime = transmission_datetime;
    }

    public String getRun_mode() {
        return run_mode;
    }

    public void setRun_mode(String run_mode) {
        this.run_mode = run_mode;
    }


    public String getOperation_mode() {
        return operation_mode;
    }

    public void setOperation_mode(String operation_mode) {
        this.operation_mode = operation_mode;
    }

    public String getChannel_version() {
        return channel_version;
    }

    public void setChannel_version(String channel_version) {
        this.channel_version = channel_version;
    }

    public String getFunction_code() {
        return function_code;
    }

    public void setFunction_code(String function_code) {
        this.function_code = function_code;
    }

    public String getFunction_sub_code() {
        return function_sub_code;
    }

    public void setFunction_sub_code(String function_sub_code) {
        this.function_sub_code = function_sub_code;
    }


}
