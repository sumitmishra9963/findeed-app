package com.findeed.KYC.VideoIdKycRes;

public class ResponseData {
    KhosalaResponse response_data;
    KhosalaResponseStatus response_status;

    public KhosalaResponse getResponse_data() {
        return response_data;
    }

    public void setResponse_data(KhosalaResponse response_data) {
        this.response_data = response_data;
    }

    public KhosalaResponseStatus getResponse_status() {
        return response_status;
    }

    public void setResponse_status(KhosalaResponseStatus response_status) {
        this.response_status = response_status;
    }
}
