package com.findeed.KYC.VideoIdKycRes;

public class Photo {
    String match_rate;
    String document_image;

    public String getMatch_rate() {
        return match_rate;
    }

    public void setMatch_rate(String match_rate) {
        this.match_rate = match_rate;
    }

    public String getDocument_image() {
        return document_image;
    }

    public void setDocument_image(String document_image) {
        this.document_image = document_image;
    }

    public String getLive_image() {
        return live_image;
    }

    public void setLive_image(String live_image) {
        this.live_image = live_image;
    }

    public String getMatch_status() {
        return match_status;
    }

    public void setMatch_status(String match_status) {
        this.match_status = match_status;
    }

    String live_image;
    String match_status;
}
