package com.findeed.KYC.VideoIdKycRes;

public class KhosalaResponse {
    String kyc_info;
    String encrypted;
    String hash;

    public String getEncrypted() {
        return encrypted;
    }

    public void setEncrypted(String encrypted) {
        this.encrypted = encrypted;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getKyc_info() {
        return kyc_info;
    }

    public void setKyc_info(String kyc_info) {
        this.kyc_info = kyc_info;
    }
}
