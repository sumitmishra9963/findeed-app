package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class IntroOne extends AppCompatActivity {

    TextView tvgetstart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_one);

        tvgetstart = findViewById(R.id.tv_getstart);

        tvgetstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroOne.this, PhoneRegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
