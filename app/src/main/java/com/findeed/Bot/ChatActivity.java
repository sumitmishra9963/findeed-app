package com.findeed.Bot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.Bot.Adapters.ChatAdapter;
import com.findeed.Bot.Adapters.ListViewAdapter;
import com.findeed.Bot.Adapters.SchoolNames;
import com.findeed.Bot.Adapters.SpinnerAdapter;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.ChargeVerifyOTP;
import com.findeed.KYC.VideoIdKYCREq.ApiData;
import com.findeed.KYC.VideoIdKycRes.KYCInfo;
import com.findeed.KYC.VideoIdKycRes.ResponseData;
import com.findeed.KYC.VideoIdKYCREq.headers;
import com.findeed.KYC.VideoIdKYCREq.request;
import com.findeed.Otp.OtpResponse;
import com.findeed.PreKYC.AddNewSchool;
import com.findeed.PreKYC.Customer;
import com.findeed.PreKYC.CustomerLoan;
import com.findeed.PreKYC.LoanType;
import com.findeed.R;
import com.google.gson.Gson;
import com.khoslalabs.base.ViKycResults;
import com.khoslalabs.facesdk.FaceSdkModuleFactory;
import com.khoslalabs.ocrsdk.OcrSdkModuleFactory;
import com.khoslalabs.videoidkyc.ui.init.VideoIdKycInitActivity;
import com.khoslalabs.videoidkyc.ui.init.VideoIdKycInitRequest;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import static com.findeed.Bot.ChatPresenter.setImage;


public class ChatActivity extends AppCompatActivity implements ChatContract.View ,AdapterView.OnItemSelectedListener{
    private static int LOAN_TYPE;

    // View
    private RecyclerView rvChatList;
    private Button btneducational, btnpersonal;
    private ChatAdapter chatAdapter;
    private ArrayList<ChatObject> chatObjects;
    // Presenter
    private ChatPresenter presenter;
    public static AutoCompleteTextView actv;
    private ListView listv;
    private LinearLayout lvlay;
    public static  LinearLayout llayoutchildcount;
    LinearLayout llayoutchildmore;
    public static LinearLayout llayoutseekbar;
    TextView txtone, txttwo, txtmoretwo, txtthree, txtfour, txtfifth;
    TextView txtseekbar, textaddseekbar;
    SeekBar simpleseekbar;
    public static LinearLayout loanType;
    boolean isOpen=false;

    private EditText  firstname2, surname2;
    private Spinner spinner2;
    public  static LinearLayout childLayout;

    public static   LinearLayout   llayoutwhatyoudo, panCardLayout;
    Button btn_workforsomeone, btn_selfemployee, btn_nowork;
    public  static Button addChild;
    public  static TextView childHeader;
    public static String userId;
    Context ctx;

    LinearLayout llayoutneedhelp, llayoutcallus, llayoutoption, llayoutnoidoption, llayoutaadharpicture, llayoutuploadaadharpicture, llayoutprocessingaadhar, llayoutaadhardetailedit, llayoutoptionselectforvoterid, llayoutuploadvoterpic, llayoutprocessingvoterid, llayoutvoterdetailedit, llayoutagreeorcancel, llayoutyesorno, llayoutAdditionalInfo, llayoutAddressManually, llayoutFillAddressManually, llayoutChooseType, llayoutVoucherVerification;

    Button  btncontinueaadhar;
    ImageView  arrowprocessingaadhar, arrowprocessingvoterid;
    Button btneditinfo, btnconfirm, btnaadhar, btnvoterid, btntakeaadharphoto, btnpickaadharphoto, btneditinfoaadhar, btnidconfirmaadhar;
    TextView txtneedhelp, txtnooption;
    Button takevoterphoto, pickvoterphoto, btncontinuevoterid, imgvoterid, btneditvoterid, btnconfirmvoterid, btnagree, btncancel, btnyes, btnno, btnMoreInfoYes, btnMoreInfoNo, btnManually, btnCurrentLocation, btnAddManuallyAdd, btnESign;
    ImageView imagechoose;
    TextView txtGreenAmber, txtNoGreen, txtRed;
    NestedScrollView scrollViewLoanChoice, scrollViewApprovedLoan;
    Button saveMyChoice, btnFileUpload, btnMonthlyEmi, btnWeeklyEmi, btnSelectVoucher, btnVoucherContinue, btnVideoRecording, btnVideoContinue, btnVoucherVerification, btnNotifySecond, btnAgreeandContinue, btnAgreeandContinueTwo;
    TextView textStartOver;

    Button iHavePan,iDontHavePan;
    public  static HashMap<String,View> viewList= new HashMap<>();
    String clientCode="DEVK1488";
    String apiKey="d89h25j8";
    String requestId="DEVKRAFT123";
    String salt="h7x9s2nka";
    String purpose="testing";
    public  String HASH;

  // public final MediaPlayer mp = MediaPlayer.create(this, R.raw.accomplished);

    public String className;
    private   void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bot_main_activity);
        new GetSchoolList().execute();

        String hashSequence=clientCode+"|"+requestId+"|"+apiKey+"|"+salt;


        HASH = sha256(hashSequence);


        final VideoIdKycInitRequest videoIdKycInitRequest =
                new VideoIdKycInitRequest
                        .Builder(
                        this.clientCode,
                        this.apiKey,
                        this.purpose,
                        this.requestId,
                        this.HASH)
                        .moduleFactory(OcrSdkModuleFactory.newInstance())
                        .moduleFactory(FaceSdkModuleFactory.newInstance())
                        .build();



        loanType=findViewById(R.id.loanSelection);
        llayoutchildcount = findViewById(R.id.llchildcount);
        llayoutchildmore = findViewById(R.id.llchildtwoplus);
        txtone = findViewById(R.id.circle_one);
        txttwo = findViewById(R.id.circle_two);
        txtmoretwo = findViewById(R.id.circle_twoplus);
        txtthree = findViewById(R.id.circle_three);
        txtfour = findViewById(R.id.circle_four);
        txtfifth = findViewById(R.id.circle_fifth);
        llayoutseekbar =  findViewById(R.id.llseekbar);
        simpleseekbar = findViewById(R.id.simpleSeekBar);
        textaddseekbar = findViewById(R.id.txt_add_seekbar);
        llayoutwhatyoudo = findViewById(R.id.llwhatdoyou);
        btn_workforsomeone = findViewById(R.id.btn_workforsomeone);
        btn_selfemployee = findViewById(R.id.btn_selfemployed);
        btn_nowork = findViewById(R.id.btn_notworking);
        rvChatList =  findViewById(R.id.rv_chat);
        actv =  findViewById(R.id.actv);
        lvlay =  findViewById(R.id.lvlay);
        Button lvbutn = findViewById(R.id.lvbutn);
        iHavePan=findViewById(R.id.btn_pancard);
        iDontHavePan=findViewById(R.id.btn_nopancard);

        childLayout =findViewById(R.id.lvchild2);
        panCardLayout=findViewById(R.id.panCradLinerLayout);

        btneducational =  findViewById(R.id.btneducational);
        btnpersonal =  findViewById(R.id.btnpersonal);

        btneditinfo = findViewById(R.id.btn_editinfo);
        btnconfirm = findViewById(R.id.btn_idconfirm);
        llayoutneedhelp = findViewById(R.id.llneedhelp);
        txtneedhelp = findViewById(R.id.needhelp);
        llayoutcallus = findViewById(R.id.llcallus);
        llayoutnoidoption = findViewById(R.id.llnoidoptiontext);
        llayoutoption = findViewById(R.id.llnooptionselect);
        btnaadhar = findViewById(R.id.btn_aadhar);
        btnvoterid = findViewById(R.id.btn_voterid);
        txtnooption = findViewById(R.id.txtnooption);
        llayoutaadharpicture = findViewById(R.id.lloptionselect);
        btntakeaadharphoto = findViewById(R.id.btn_takeaadharphoto);
        btnpickaadharphoto = findViewById(R.id.btn_pickaadharphoto);
        llayoutuploadaadharpicture = findViewById(R.id.lluploadpaadharpic);
        btncontinueaadhar = findViewById(R.id.btn_continue_aadhar);
        llayoutprocessingaadhar = findViewById(R.id.llprocessingaadhar);
        arrowprocessingaadhar = findViewById(R.id.arrow_processingaadhar);
        llayoutaadhardetailedit =findViewById(R.id.llaadhardetailedit);
        btnidconfirmaadhar = findViewById(R.id.btn_idconfirmaadhar);
        btneditinfoaadhar = findViewById(R.id.btn_editinfoaadhar);
        llayoutoptionselectforvoterid = findViewById(R.id.lloptionselectforvoterid);
        takevoterphoto = findViewById(R.id.btn_takeavoterphoto);
        pickvoterphoto = findViewById(R.id.btn_pickvoterphoto);
        llayoutuploadvoterpic = findViewById(R.id.lluploadvoteridpic);
        btncontinuevoterid = findViewById(R.id.btn_continue_voterid);
        llayoutprocessingvoterid = findViewById(R.id.llprocessingvoterid);
        arrowprocessingvoterid = findViewById(R.id.arrow_processingvoterid);
        llayoutvoterdetailedit = findViewById(R.id.llvoterdetailedit);
        btneditvoterid = findViewById(R.id.btn_editinfovoterid);
        btnconfirmvoterid = findViewById(R.id.btn_idconfirmvoterid);
        llayoutagreeorcancel = findViewById(R.id.llagreeorcancel);
        btnagree = findViewById(R.id.btn_agree);
        btncancel = findViewById(R.id.btn_cancel);
        llayoutyesorno = findViewById(R.id.llyesorno);
        btnyes = findViewById(R.id.btn_yes);
        btnno = findViewById(R.id.btn_no);
        llayoutAdditionalInfo = findViewById(R.id.ll_Additional_Info);
        btnMoreInfoYes = findViewById(R.id.btn_moreinfo_yes);
        btnMoreInfoNo = findViewById(R.id.btn_moreinfo_no);
        llayoutAddressManually = findViewById(R.id.ll_Address_Manually);
        btnCurrentLocation = findViewById(R.id.btn_currentlocation);
        btnManually = findViewById(R.id.btn_manually);
        llayoutFillAddressManually = findViewById(R.id.llfilladdressmanually);
        btnAddManuallyAdd = findViewById(R.id.btn_add_address_manually);
        llayoutChooseType = findViewById(R.id.llchoosetype);
        imagechoose = findViewById(R.id.img_cancel_choose);
        txtGreenAmber = findViewById(R.id.txt_green_amber);
        txtNoGreen = findViewById(R.id.txt_green_no);
        txtRed = findViewById(R.id.txt_red);
        scrollViewLoanChoice = findViewById(R.id.scroll_loan_choice);
        saveMyChoice = findViewById(R.id.btn_savemychoice);
        btnFileUpload = findViewById(R.id.btn_fileUpload);
        btnMonthlyEmi = findViewById(R.id.btn_monthlyEmi);
        btnWeeklyEmi = findViewById(R.id.btn_weeklyEmi);
        btnSelectVoucher = findViewById(R.id.btn_select_Voucher);
        btnVoucherContinue = findViewById(R.id.btn_VoucherContinue);
        btnVideoRecording = findViewById(R.id.btn_videoRecording);
        btnVideoContinue = findViewById(R.id.btn_videoContinue);
        btnVoucherVerification = findViewById(R.id.btn_voucherVerification);
        llayoutVoucherVerification = findViewById(R.id.llvoucherVerification);
        btnNotifySecond = findViewById(R.id.btn_NofifySecond);
        btnESign = findViewById(R.id.btn_ESign);
        scrollViewApprovedLoan = findViewById(R.id.SViewApprovedLoan);
        btnAgreeandContinue = findViewById(R.id.btn_AgreeandContinue);
        btnAgreeandContinueTwo = findViewById(R.id.btn_AgreeandContinuetwo);
        textStartOver = findViewById(R.id.txt_startOver);




        viewList.put("Hello,Good Morning What type of loan are you looking out for?",loanType);
        viewList.put("Which school does your child goes to ?",actv);
        viewList.put("How many of your children study in ",llayoutchildcount);
        viewList.put("How much loan do you require?",llayoutseekbar);
        viewList.put("So what you do?",llayoutwhatyoudo);
        viewList.put("Can you provide us your PAN card for ID verification?",panCardLayout);
        firstname2 =  findViewById(R.id.firstname2);
        surname2 =  findViewById(R.id.surname2);
        spinner2 =  findViewById(R.id.spinner2);
        addChild = findViewById(R.id.childBt2);
        childHeader=findViewById(R.id.addChildHeader);

        ctx=getApplicationContext();

        this.chatObjects = new ArrayList<>();
        btneducational.setOnClickListener(v -> {
            presenter.onEditTextActionDone("Educational Loan");
            //  scrollChatDown();
            btneducational.setVisibility(View.GONE);
            btnpersonal.setVisibility(View.GONE);
            LOAN_TYPE=1;
            new createCustomerLoan().execute();


        });

        btnpersonal.setOnClickListener(v -> {
            presenter.onEditTextActionDone("Personal Loan");
            // scrollChatDown();
            btneducational.setVisibility(View.GONE);
            btnpersonal.setVisibility(View.GONE);
            LOAN_TYPE=2;
            new createCustomerLoan().execute();
        });



        btneditinfo.setOnClickListener(v -> {
            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.pan_card_dialog);

            Button btnsavedetail = dialog.findViewById(R.id.btn_savedetail);

            btnsavedetail.setOnClickListener(v12 -> {


                panCardLayout.setVisibility(View.GONE);

                dialog.dismiss();

            });


            dialog.show();
        });




        btnconfirm.setOnClickListener(v -> {

            panCardLayout.setVisibility(View.GONE);
            presenter.botMessage("Sorry, looks like the information you provided for pan card are not correct.");
            llayoutneedhelp.setVisibility(View.VISIBLE);

        });

        txtneedhelp.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_call_us);

            final LinearLayout layoutcallus = dialog.findViewById(R.id.llcallus);
            layoutcallus.setOnClickListener(v13 -> {
                layoutcallus.setVisibility(View.GONE);
                llayoutneedhelp.setVisibility(View.GONE);
                dialog.dismiss();
                presenter.botMessage("Awesome now we have your ID proof.");
                presenter.botMessage("Can you provide your address proof? You can give us your Aadhar or Voter's ID card. Which one would you like  to use?");
                llayoutnoidoption.setVisibility(View.VISIBLE);
                llayoutoption.setVisibility(View.VISIBLE);

            });

            dialog.show();

        });

        txtnooption.setOnClickListener(v -> {

            llayoutnoidoption.setVisibility(View.GONE);
            presenter.onEditTextActionDone("We are waiting for your details. Please select one option. If you currently don't have a proof, we will wait for you to come back with the details. Need any help?(https://projects.invisionapp.com/d/main#/console/18327634/382471876/comments/121064872)");

            llayoutoption.setVisibility(View.VISIBLE);
        });


        btnaadhar.setOnClickListener(v -> {

            llayoutnoidoption.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Aadhar Card");
            llayoutoption.setVisibility(View.GONE);
            presenter.botMessage("Can you provide your Aadhar card for verification?");

            llayoutaadharpicture.setVisibility(View.VISIBLE);


        });


        btnvoterid.setOnClickListener(v -> {

            llayoutnoidoption.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Voter's ID");
            llayoutoption.setVisibility(View.GONE);
            presenter.botMessage("Can you provide your Voter's ID for verification?");
            llayoutoptionselectforvoterid.setVisibility(View.VISIBLE);




        });

        btntakeaadharphoto.setOnClickListener(v -> {

            llayoutuploadaadharpicture.setVisibility(View.VISIBLE);
            llayoutaadharpicture.setVisibility(View.GONE);





        });
        btnpickaadharphoto.setOnClickListener(v -> {

        });


        takevoterphoto.setOnClickListener(v -> {
            llayoutoptionselectforvoterid.setVisibility(View.GONE);
            llayoutuploadvoterpic.setVisibility(View.VISIBLE);


        });

        pickvoterphoto.setOnClickListener(v -> {


        });



        btncontinueaadhar.setOnClickListener(v -> {

            llayoutuploadaadharpicture.setVisibility(View.GONE);
            llayoutprocessingaadhar.setVisibility(View.VISIBLE);

        });



        btncontinuevoterid.setOnClickListener(v -> {

            llayoutuploadvoterpic.setVisibility(View.GONE);
            llayoutprocessingvoterid.setVisibility(View.VISIBLE);

        });



        arrowprocessingaadhar.setOnClickListener(v -> {

            llayoutprocessingaadhar.setVisibility(View.GONE);
            presenter.botMessage("Here are your details in the Aadhar Card");
            presenter.botMessage("Aadhar Number - 123456789101 \n Name - Rahul Kumar, Date of birth - 27th December 1978" );
            llayoutaadhardetailedit.setVisibility(View.VISIBLE);

        });

        arrowprocessingvoterid.setOnClickListener(v -> {

            llayoutprocessingvoterid.setVisibility(View.GONE);
            presenter.botMessage("Here are your details in the Voter's ID Card");
            presenter.botMessage("Voter ID Number - YUF123456 \n Name - Rahul Kumar, Date of birth - 27th December 1978" );
            llayoutvoterdetailedit.setVisibility(View.VISIBLE);

        });



        btneditinfoaadhar.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_editaadhardetail);

            Button btnsaveaadhardetail = dialog.findViewById(R.id.btn_saveaadhardetail);
            btnsaveaadhardetail.setOnClickListener(v1 -> dialog.dismiss());

            dialog.show();


        });
        btnidconfirmaadhar.setOnClickListener(v -> {


            presenter.botMessage("Thank you  for providing the ID and Address proofs.");
            presenter.botMessage("we will forward these details to the Bureau for Varification.");
            llayoutaadhardetailedit.setVisibility(View.GONE);
            llayoutagreeorcancel.setVisibility(View.VISIBLE);


        });

        btneditvoterid.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custome_dialog_editvoterdetail);

            Button btnsavevoterdetail = dialog.findViewById(R.id.btn_saveavoterdetail);
            btnsavevoterdetail.setOnClickListener(v15 -> dialog.dismiss());

            dialog.show();

        });



        btnconfirmvoterid.setOnClickListener(v -> {

            llayoutvoterdetailedit.setVisibility(View.GONE);

            presenter.botMessage("Thank you  for providing the ID and Address proofs.");
            presenter.botMessage("we will forward these details to the Bureau for Varification.");
            llayoutagreeorcancel.setVisibility(View.VISIBLE);


        });


        btnagree.setOnClickListener(v -> {
            llayoutagreeorcancel.setVisibility(View.GONE);
            presenter.botMessage("Is the address in your Aadhar card your current address?");
            llayoutyesorno.setVisibility(View.VISIBLE);


        });

        btncancel.setOnClickListener(v -> {


        });

        btnyes.setOnClickListener(v -> {

            llayoutyesorno.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Yes");
            presenter.botMessage("We would require some additional information from you for smooth processing of your loan application");
            llayoutAdditionalInfo.setVisibility(View.VISIBLE);

        });



        btnno.setOnClickListener(v -> {
            llayoutyesorno.setVisibility(View.GONE);
            presenter.onEditTextActionDone("No");
            llayoutAddressManually.setVisibility(View.VISIBLE);

        });

        btnCurrentLocation.setOnClickListener(v -> {

            llayoutAddressManually.setVisibility(View.GONE);
            //Give Permission
        });

        btnManually.setOnClickListener(v -> {
            llayoutAddressManually.setVisibility(View.GONE);
            presenter.botMessage("Can you please complete the address below?");
            llayoutFillAddressManually.setVisibility(View.VISIBLE);


        });



        btnAddManuallyAdd.setOnClickListener(v -> {
            llayoutFillAddressManually.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Your Address is Bangalore");
            llayoutAdditionalInfo.setVisibility(View.VISIBLE);
            presenter.botMessage("Ok! We would now be asking your permission to access your call logs, contacts, and sms. Would you like to proceed?");
        });



        btnMoreInfoYes.setOnClickListener(v -> {
            llayoutAdditionalInfo.setVisibility(View.GONE);
            Toast.makeText(ChatActivity.this, "Allow Contacts, Call permissions", Toast.LENGTH_SHORT).show();
            presenter.botMessage("Thank You!");


            new Handler().postDelayed(() -> presenter.botMessage("We will verify the details provided by you and notify you can once the loan    is approved."), 1000);

            new Handler().postDelayed(() -> presenter.botMessage("Please get back when you receive notification from us."), 1000);

            llayoutChooseType.setVisibility(View.VISIBLE);

        });

        btnMoreInfoNo.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_moreinfo);
            TextView textView = dialog.findViewById(R.id.goback);
            textView.setOnClickListener(v17 -> {

                dialog.dismiss();

                llayoutAdditionalInfo.setVisibility(View.VISIBLE);

            });

            dialog.show();

        });


        imagechoose.setOnClickListener(v -> llayoutChooseType.setVisibility(View.GONE));
        txtGreenAmber.setOnClickListener(v -> {
            llayoutChooseType.setVisibility(View.GONE);
            presenter.botMessage("Congratulations! You are eligible for a loan amount of upto Rs. 40,000");
            presenter.botMessage("Please select the loan tenure and frequency of payment.");
            scrollViewLoanChoice.setVisibility(View.VISIBLE);

        });
        txtNoGreen.setOnClickListener(v -> {
            llayoutChooseType.setVisibility(View.GONE);
            presenter.botMessage("Congratulations! You are eligible for a loan amount of upto Rs. 40,000");
            presenter.botMessage("Please select the loan tenure and frequency of payment.");
            scrollViewLoanChoice.setVisibility(View.VISIBLE);

        });



        txtRed.setOnClickListener(v -> {
            llayoutChooseType.setVisibility(View.GONE);
            presenter.botMessage("Congratulations! You are eligible for a loan amount of upto Rs. 40,000");
            presenter.botMessage("Please select the loan tenure and frequency of payment.");
            scrollViewLoanChoice.setVisibility(View.VISIBLE);

        });

        saveMyChoice.setOnClickListener(v -> {
            scrollViewLoanChoice.setVisibility(View.GONE);
            presenter.botMessage("Can you provide some more information to process your loan application?");
            presenter.botMessage("please upload your bank statement or passbook for the last 3 months");
            btnFileUpload.setVisibility(View.VISIBLE);
        });

        btnFileUpload.setOnClickListener(v -> {

            btnFileUpload.setVisibility(View.GONE);
            Toast.makeText(ChatActivity.this, "Here Upload Your Documents", Toast.LENGTH_SHORT).show();
            presenter.botMessage("Thankyou for providing bank details");


            Handler handler = new Handler();
            handler.postDelayed(() -> {
                presenter.botMessage("Can you provide us 2 people who can stand in for you?");
                btnSelectVoucher.setVisibility(View.VISIBLE);
            }, 2000);

        });

        btnMonthlyEmi.setOnClickListener(v -> {






        });
        btnWeeklyEmi.setOnClickListener(v -> {



        });


        btnSelectVoucher.setOnClickListener(v -> {

            Toast.makeText(ChatActivity.this, "Here Open List Of Contacts", Toast.LENGTH_SHORT).show();
            btnSelectVoucher.setVisibility(View.GONE);
            presenter.botMessage("We have sent a request via Whatsapp & SMS to the references you have selected. Please ask your references to check for the notification and provide the details. This is important to process your application.");
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                presenter.botMessage("Voucher 1: \n Name - XYZ \n Contact No. - 8181818181  \n \"Voucher 2: \n Name - XYZ \n Contact No. - 8181818181");
                btnSelectVoucher.setVisibility(View.GONE);
                btnVoucherContinue.setVisibility(View.VISIBLE);
            }, 2000);

        });

        btnVoucherContinue.setOnClickListener(v -> {
            btnVoucherContinue.setVisibility(View.GONE);
            presenter.botMessage("Congratulations, we have just one more step to go. Lets complete a video call and we are all set.");
            btnVideoRecording.setVisibility(View.VISIBLE);
        });

        btnVideoRecording.setOnClickListener(v -> {

            btnVideoRecording.setVisibility(View.GONE);
            presenter.botMessage("Congratulatins! Vouchers have been verified!");
            presenter.botMessage("Both the vouchers have completed the vouching process. We are happy to take your loan application forward.");
            btnVideoContinue.setVisibility(View.VISIBLE);
        });

        btnVideoContinue.setOnClickListener(v -> {
            btnVideoContinue.setVisibility(View.GONE);
            btnVoucherVerification.setVisibility(View.VISIBLE);
            presenter.botMessage("Congratulations! Your first Voucher have been verified but second one is yet to be verified.");
        });

        btnVoucherVerification.setOnClickListener(v -> {
            btnVoucherVerification.setVisibility(View.GONE);

            llayoutVoucherVerification.setVisibility(View.VISIBLE);


        });

        btnNotifySecond.setOnClickListener(v -> {

            llayoutVoucherVerification.setVisibility(View.GONE);

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_verification);

            Button btnTakePicVerify = dialog.findViewById(R.id.btn_takephotoverify);
            Button btnPickPicVerify = dialog.findViewById(R.id.btn_pick_photoverify);


            btnTakePicVerify.setOnClickListener(v16 -> {
                btnNotifySecond.setVisibility(View.GONE);
                presenter.botMessage("Sorry, your loan application cannot be processed at this point of time.");
                presenter.botMessage("Please get back to us with the correct proofs to proceed further.");
                dialog.dismiss();

                Handler handler = new Handler();
                handler.postDelayed(() -> btnNotifySecond.setVisibility(View.VISIBLE), 2000);

            });



            btnPickPicVerify.setOnClickListener(v14 -> {

                btnNotifySecond.setVisibility(View.GONE);
                presenter.botMessage("Thank You!");
                presenter.botMessage("Lorem ipsum dolor sit amet");
                dialog.dismiss();
                btnESign.setVisibility(View.VISIBLE);
            });

            dialog.show();




        });

        btnESign.setOnClickListener(v -> {

            btnESign.setVisibility(View.GONE);
            scrollViewApprovedLoan.setVisibility(View.VISIBLE);
        });

        btnAgreeandContinue.setOnClickListener(v -> {
            scrollViewApprovedLoan.setVisibility(View.GONE);
            presenter.botMessage("Findeed will be charging Rs. X fee for processing your loan application.");
            btnAgreeandContinueTwo.setVisibility(View.VISIBLE);

        });
        btnAgreeandContinueTwo.setOnClickListener(v -> {

            Intent intent = new Intent(ChatActivity.this, ChargeVerifyOTP.class);
            startActivity(intent);

        });

        textStartOver.setOnClickListener(v -> {
            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_startover);

            // TextView txtYesStartOver = dialog.findViewById(R.id.txt_startOver);
            // TextView txtNoStartOver = dialog.findViewById(R.id.txt_cancelStartOver);

            dialog.show();

        });




        txtone.setOnClickListener(v -> {
            // scrollChatDown();
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            presenter.onEditTextActionDone("1 Children");
            //  scrollChatDown();
            childLayout.setVisibility(View.VISIBLE);
//            final View childView = findViewById(R.id.lvchild2);
            addChild.setOnClickListener(view -> {
                //  scrollChatDown();
                childLayout.setVisibility(View.GONE);
                childClick(childLayout,1,firstname2,surname2,className);
            });
        });



        txttwo.setOnClickListener(v -> {
            // scrollChatDown();
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            presenter.onEditTextActionDone("2 Children");
            // scrollChatDown();
            childLayout.setVisibility(View.VISIBLE);
//            final View childView = findViewById(R.id.lvchild2);
            addChild.setOnClickListener(view -> childClick(childLayout,2,firstname2,surname2,className));
        });


        txtmoretwo.setOnClickListener(view -> {
            if(!isOpen){
                llayoutchildmore.setVisibility(View.VISIBLE);
                isOpen=true;
            }else{
                llayoutchildmore.setVisibility(View.GONE);
                isOpen=false;
            }


        });


        txtthree.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            presenter.onEditTextActionDone("3 Children");
            // scrollChatDown();
            childLayout.setVisibility(View.VISIBLE);
            //  final View childView = findViewById(R.id.lvchild2);
            addChild.setOnClickListener(view -> childClick(childLayout,3,firstname2,surname2,className));
        });
//
//
//
        txtfour.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            presenter.onEditTextActionDone("4 Children");
            //scrollChatDown();
            childLayout.setVisibility(View.VISIBLE);
            // final View childView = findViewById(R.id.lvchild2);
            addChild.setOnClickListener(view -> childClick(childLayout,4,firstname2,surname2,className));
        });
        txtfifth.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            presenter.onEditTextActionDone("5 Children");
            // scrollChatDown();
            childLayout.setVisibility(View.VISIBLE);
            addChild.setOnClickListener(view -> childClick(childLayout,5,firstname2,surname2,className));

        });




        simpleseekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                txtseekbar = findViewById(R.id.txt_seekbar);
//                TextView textView=findViewById(R.id.invertedTriangle);

                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                txtseekbar.setText(String.valueOf(val));
//                int x = simpleseekbar.getThumb().getBounds().left;
                txtseekbar.setX(val);



            }



            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        textaddseekbar.setOnClickListener(v -> {


            // scrollChatDown();
            hideKeyboard();
            llayoutseekbar.setVisibility(View.GONE);
            presenter.onEditTextActionDone("40000");
            presenter.botMessage("So what you do?");
           // mp.start();

        });
        btn_workforsomeone.setOnClickListener(v -> {
            // scrollChatDown();
            llayoutwhatyoudo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I work for someone or a company");
            presenter.botMessage("Can you provide us your PAN card for ID verification?");

        });


        btn_selfemployee.setOnClickListener(v -> {
            // scrollChatDown();
            llayoutwhatyoudo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I am self employed or I own a business");
            presenter.botMessage("Great! we have a few more steps to complete");
            final Handler handler = new Handler();
            handler.postDelayed(() -> presenter.botMessage("Can you provide us your PAN card for ID verification?"), 1100);


        });

        btn_nowork.setOnClickListener(view -> {
            // scrollChatDown();
            llayoutwhatyoudo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I am not Working ");
            presenter.botMessage("Great! we have a few more steps to complete");
            final Handler handler = new Handler();
            handler.postDelayed(() -> presenter.botMessage("Can you provide us your PAN card for ID verification?"), 1100);
        });


        iHavePan.setOnClickListener(view -> {
            Intent myIntent =new Intent(ChatActivity.this, VideoIdKycInitActivity.class);
            myIntent.putExtra("init_request", videoIdKycInitRequest);
            startActivityForResult(myIntent, 10);
            panCardLayout.setVisibility(View.GONE);
        });
        iDontHavePan.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.pan_card_dialog);
            TextView saveMyDeatil=dialog.findViewById(R.id.btn_savedetail);
            saveMyDeatil.setOnClickListener(view1 -> dialog.dismiss());
            dialog.show();
        });


        lvbutn.setOnClickListener(view -> {


            LayoutInflater inflater = getLayoutInflater();
            View custm = inflater.inflate(R.layout.custom_dialog, null);

            final EditText etname = custm.findViewById(R.id.et_name);
            final EditText etarea = custm.findViewById(R.id.et_area);
            final EditText etcity = custm.findViewById(R.id.et_city);
            final Button btreq = custm.findViewById(R.id.btnreq);
            final AlertDialog.Builder alert = new AlertDialog.Builder(ChatActivity.this);
            alert.setTitle("Add in the List");
            alert.setView(custm);
            //alert.setCancelable(false);
            final AlertDialog dialog = alert.create();
            btreq.setOnClickListener(view12 -> {
                String name = etname.getText().toString();
                String area = etarea.getText().toString();
                String city = etcity.getText().toString();

                if(TextUtils.isEmpty(name)||TextUtils.isEmpty(area)||TextUtils.isEmpty(city)){
                    Toast.makeText(getApplicationContext(),"Please enter required details",Toast.LENGTH_LONG).show();
                }else {

                    Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.school_image);

                    setImage(icon);
                    actv.setVisibility(View.GONE);
                    lvlay.setVisibility(View.GONE);
                    presenter.onEditTextActionDone(name+", "+area+"");
                    dialog.dismiss();
                    presenter.botMessage("How many of your children study in "+name);
                    //  scrollChatDown();
                }


            });

            dialog.show();
        });


        String[] classsArr = new String[]{"Class", "1st Standard", "2nd Standard", "3rd Standard", "4th Standard", "5th Standard", "6th Standard"};
        SpinnerAdapter spinAdp=new SpinnerAdapter(getApplicationContext(), classsArr);
        spinner2.setAdapter(spinAdp);
        spinner2.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                className=getResources().getStringArray((R.array.class_array))[spinner2.getSelectedItemPosition()];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

         /*ArrayAdapter<CharSequence> adptr= ArrayAdapter.createFromResource(this,
                R.array.class_array, android.R.layout.simple_spinner_item);
        adptr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/


     /*   spinner1.setAdapter(spinAdp);
        spinner1.setOnItemSelectedListener(this);*/
        //

        //
    /*    spinner3.setAdapter(spinAdp);
        spinner3.setOnItemSelectedListener(this);
        //
        spinner4.setAdapter(spinAdp);
        spinner4.setOnItemSelectedListener(this);
        //
        spinner5.setAdapter(spinAdp);
        spinner5.setOnItemSelectedListener(this);
        //
        spinner6.setAdapter(spinAdp);
        spinner6.setOnItemSelectedListener(this);*/





        this.presenter = new ChatPresenter();
        presenter.attachView(this);

        // Instantiate the adapter and give it the list of chat objects
        this.chatAdapter = new ChatAdapter(presenter.getChatObjects());

        // Set up the RecyclerView with adapter and layout manager

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvChatList.setAdapter(chatAdapter);
        rvChatList.setLayoutManager(linearLayoutManager);
        rvChatList.setItemAnimator(new DefaultItemAnimator());
        presenter.botMessage("Hello,Good Morning What type of loan are you looking out for?");

        rvChatList.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {

            if (bottom < oldBottom) {
                rvChatList.postDelayed(() -> rvChatList.smoothScrollToPosition(
                        rvChatList.getAdapter().getItemCount() - 1), 100);
            }
        });


    }

    public void childClick(View v, final int noOfChild, final TextView name, final TextView sname, final String className){


        if(noOfChild==1){

            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(sname.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                hideKeyboard();
                childLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone(name.getText().toString() + " " + sname.getText().toString());
                presenter.onEditTextActionDone(className);
                presenter.botMessage("How much loan do you require?");

            }

        }
        else if(noOfChild==2){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(sname.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {

                presenter.onEditTextActionDone(name.getText().toString() + " " + sname.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                firstname2.setText("");
                surname2.setText("");
                childHeader.setText("Second Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }
        else if(noOfChild==3){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(sname.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(name.getText().toString() + " " + sname.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                firstname2.setText("");
                surname2.setText("");
                childHeader.setText("Second Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }

        else if(noOfChild==4){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(sname.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(name.getText().toString() + " " + sname.getText().toString());
                childLayout.setVisibility(View.GONE);
                firstname2.setText("");
                surname2.setText("");
                childHeader.setText("Second Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }
        else {
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(sname.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(name.getText().toString() + " " + sname.getText().toString());
                childLayout.setVisibility(View.GONE);
                firstname2.setText("");
                surname2.setText("");
                childHeader.setText("Second Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }


        }
    }




    private void secondChild(final int noOfChild) {
        if(noOfChild==2) {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                hideKeyboard();
                childLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                presenter.botMessage("How much loan do you require?");
//                scrollChatDown();
            }

        }
        else if(noOfChild==3){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname=findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname=findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Third Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> thirdChild(noOfChild));
            }

        }
        else if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname=findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname=findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Third Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> {
                    childLayout.setVisibility(View.GONE);
                    thirdChild(noOfChild);
                });
            }

        }
        else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname=findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname=findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Third Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> {
                    childLayout.setVisibility(View.GONE);
                    thirdChild(noOfChild);
                });
            }

        }
    }

    private void thirdChild(final int noOfChild) {
        if(noOfChild==3){

            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                hideKeyboard();
                childLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                presenter.botMessage("How much loan do you require?");
            }

        }
        else if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname = findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname = findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Fourth Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fourthChild(noOfChild);
                    }
                });
            }

        }
        else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname = findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname = findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Fourth Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> fourthChild(noOfChild));
            }
        }
    }

    public void fourthChild(final int noOfChild){

        if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                hideKeyboard();
                childLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                presenter.botMessage("How much loan do you require?");
                //  scrollChatDown();
            }
        }else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(TextUtils.isEmpty(surname2.getText())){
                Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(className.equals("Class")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
                presenter.onEditTextActionDone(className);
                childLayout.setVisibility(View.GONE);
                final TextView fname = findViewById(R.id.firstname2);
                fname.setText("");
                final TextView sname = findViewById(R.id.surname2);
                sname.setText("");
                childHeader.setText("Fifth Child");
                childLayout.setVisibility(View.VISIBLE);
                addChild.setOnClickListener(view -> {
                    hideKeyboard();
                    //   scrollChatDown();
                    fifthChild();
                });
            }
        }
    }
    public void fifthChild(){
        if(TextUtils.isEmpty(firstname2.getText())){
            Toast.makeText(getApplicationContext(), "Name can not be empty!",
                    Toast.LENGTH_LONG).show();
            childLayout.setVisibility(View.VISIBLE);
        }else if(TextUtils.isEmpty(surname2.getText())){
            Toast.makeText(getApplicationContext(), "Surname can not be empty!",
                    Toast.LENGTH_LONG).show();
            childLayout.setVisibility(View.VISIBLE);
        }else if(className.equals("Class")){
            Toast.makeText(getApplicationContext(), "Class can not be empty!",
                    Toast.LENGTH_LONG).show();
            childLayout.setVisibility(View.VISIBLE);
        }else {
            childLayout.setVisibility(View.GONE);
            presenter.onEditTextActionDone(firstname2.getText().toString() + " " + surname2.getText().toString());
            presenter.onEditTextActionDone(className);
            presenter.botMessage("How much loan do you require?");
        }

    }






    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10){
            if (resultCode == ViKycResults.RESULT_OK
                    || resultCode == ViKycResults.RESULT_DOC_COMPLETE) {
                if (data != null) {
                    // use the user id

                    userId = data.getStringExtra("user_id");
                    new callApi().execute();
                }
            } else {
                if (data != null) {
                    // use the error code and error message
                    int code = data.getIntExtra("error_code", 333);
                    String msg = data.getStringExtra("error_message");
                    Toast.makeText(getApplicationContext(), "Error in SDK!",
                            Toast.LENGTH_LONG).show();
                }
            }

        }
    }




    @Override
    public void notifyAdapterObjectAdded(int position) {
        this.chatAdapter.notifyItemInserted(position);
    }

    @Override
    public void scrollChatDown() {
        this.rvChatList.smoothScrollToPosition(presenter.getChatObjects().size() - 1);
    }



    private EditText.OnEditorActionListener searchBoxListener = new EditText.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView tv, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!TextUtils.isEmpty(tv.getText())) {
                    listv.setVisibility(View.VISIBLE);
                    presenter.onEditTextActionDone(tv.getText().toString());
//                    etSearchBox.getText().clear();
                    return true;
                }
            }
            return false;
        }
    };



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


/*This async task is being called to fetch kyc details from Khosala Lab*/
    private class callApi extends AsyncTask<String,String,String>{

        String seq=clientCode+"|"+userId+"|"+apiKey+"|"+salt;
        String eKYCHash=sha256(seq);



        @Override
        protected void onPostExecute( final String s) {

            runOnUiThread(() ->
                    presenter.onEditTextActionDone(s)
            );
            runOnUiThread(() -> presenter.botMessage("Thank You, your pan details have been submitted successfully"));
            super.onPostExecute(s);

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(String... strings) {
            Random rand = new Random();
            headers h = new headers();
            h.setClient_code(clientCode);
            h.setSub_client_code("dummy-code");
            h.setChannel_code("ANDROID_SDK");
            h.setChannel_version("1.0.0");
            h.setTransmission_datetime("1553255080298");
            h.setOperation_mode("SELF");
            h.setRun_mode("TRAIL");
            h.setActor_type("OTHER");
            h.setUser_handle_type("EMAIIL");
            h.setUser_handle_value("a@b.com");
            h.setFunction_code("DEFAULT");
            h.setFunction_sub_code("DEFAULT");
            h.setStan(String.valueOf(rand.nextInt(50)));

            request re = new request();
            re.setApi_key(apiKey);
            re.setRequest_id(requestId);
            re.setHash(eKYCHash);
            re.setUser_id(userId);

            ApiData ad = new ApiData();
            ad.setHeaders(h);
            ad.setRequest(re);
            Gson gson = new Gson();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            String jsonData=gson.toJson(ad);
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url("https://sandbox.veri5digital.com/video-id-kyc/api/1.0/fetchKYCInfo")
                    .post(body)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                ResponseData responseData=gson.fromJson(resData,ResponseData.class);
                byte [] decodedString= Base64.getDecoder().decode(responseData.getResponse_data().getKyc_info());
                String decoded = new String(decodedString);
                KYCInfo kyc_info =gson.fromJson(decoded,KYCInfo.class);
                String userInfo=  "Name: "+kyc_info.getDeclared_kyc_info().getName()+"\n"+"Father's Name: "+kyc_info
                        .getDeclared_kyc_info().getFather_name()+"\n"+"DOB: "+kyc_info.getDeclared_kyc_info().getDob()+"\n"
                        +"Document: "+kyc_info.getDeclared_kyc_info().getDoc_type();
                return  userInfo;


            } catch (IOException e) {
                e.printStackTrace();
                return null;

            }catch (RuntimeException r){
                r.printStackTrace();
                return null;
            }

        }
    }


    /*This async task will be called to store the type of loan user have selected*/

    public class createCustomerLoan extends AsyncTask<String,String,String>{

        @Override
        protected void onPostExecute(String s) {
            runOnUiThread(() -> presenter.botMessage("Which school does your child goes to ?"));
        }

        @Override
        protected String doInBackground(String... strings) {

            Customer c1= new Customer();
            c1.setCustomerId("1");
            LoanType l1 = new LoanType();
            l1.setLoanTypeId(String.valueOf(LOAN_TYPE));
            CustomerLoan cl = new CustomerLoan();
            cl.setCustomer(c1);
            cl.setLoanType(l1);
            Gson gson = new Gson();
            String jsonData=gson.toJson(cl);
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/customer/createCustomerLoan")
                    .post(body)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    /*This async task will be called when we need to populate the school names in list view */
    public class GetSchoolList extends AsyncTask<String, String ,String>{

        @Override
        protected void onPostExecute(String s) {
            runOnUiThread(() -> {
                Map<String, String> map = new HashMap<>();

                map.put("Amity Public School","Bangalore, 664521");
                map.put("Abingdon High School","Pune, 110235");
                map.put("Albion High School","Dehradun,548975");
                map.put("Bellmont High School","Delhi, 110025");
                map.put("Bowen High School","Sonipat, 845789");
                map.put("Burlington High School","Chennai, 214578");
                map.put("Fairbury-Cropsey HS","Guana, 5894789");
                map.put("Flat Rock High School","Vanta, 5484566");
                map.put("Royal High School","Delhi, 113311");
                map.put("Happy public school","Mumbai, 445874");
                map.put("Dayanand Public School","Chennai, 234798");
                map.put("Delhi Public School","Hyderabad, 512369");
                map.put("Blue Bells Public School","Pune, 587469");
                map.put("Zeaneth Public School","Kolkatta, 125485");

                listv = findViewById(R.id.listview);
                final ArrayList<SchoolNames> arraylist = new ArrayList<>();

    /*for (int i = 0; i < arr.length; i++) {
        arraylist.add(new SchoolNames(arr[i]));
    }*/

                for (Map.Entry<String, String> entry : map.entrySet()) {
                    arraylist.add(new SchoolNames(entry.getKey(), entry.getValue()));

                }

                final ListViewAdapter adapter = new ListViewAdapter(ctx, arraylist);
                listv.setAdapter(adapter);



                actv.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                    }
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        lvlay.setVisibility(View.VISIBLE);
                        String cseq = "" + charSequence;
                        adapter.filter(cseq);
                    }
                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                listv.setOnItemClickListener((parent, view, position, id) -> {
                    hideKeyboard();
                    lvlay.setVisibility(View.GONE);
                    actv.setVisibility(View.GONE);
                    String schoolname = arraylist.get(position).getSchoolname();
                    presenter.onEditTextActionDone(schoolname);
                    presenter.botMessage("How many of your children study in " + schoolname+" ?");

                });
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/org/getSchools")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }


        }
    }


    /*This async task will be called when user will click on add in list button for school*/

    public class CreateSchool extends AsyncTask<String,String,String >{
        LayoutInflater inflater = getLayoutInflater();
        View custm = inflater.inflate(R.layout.custom_dialog, null);
        @Override
        protected String doInBackground(String... strings) {
            final EditText etname = custm.findViewById(R.id.et_name);
            final EditText etarea = custm.findViewById(R.id.et_area);
            final EditText etcity = custm.findViewById(R.id.et_city);
            AddNewSchool as = new AddNewSchool();
            as.setSchoolName(etname.getText().toString());
            as.setArea(etarea.getText().toString());
            as.setCity(etcity.getText().toString());
            Gson gson = new Gson();
            String jsonData=gson.toJson(as);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                                .url("http://157.245.102.194:8080/los/org/createSchool")
                                .post(body)
                                .build();
            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    /*This async Task will be called when we have to store number of children user selected */

    public class NumberOfChildren extends AsyncTask<String ,String, String >{
        String customerId,number;

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/numberOfChildren/"+customerId+"/"+number)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }

    /*This async Task will be called when we have to store the amount of loan user want*/

    public class LoanRequirement extends AsyncTask<String, String ,String >{
        String customerId,amount;
        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/loanAmount/"+customerId+"/"+amount)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    /*This async Task will be called when we have to store the employment type of the user*/

    public class EmploymentType extends AsyncTask<String, String ,String >{
        String customerId,type;
        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/employmentType/"+customerId+"/"+type)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

}