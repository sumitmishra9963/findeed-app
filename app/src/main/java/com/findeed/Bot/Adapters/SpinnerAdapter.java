package com.findeed.Bot.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.findeed.R;

public class SpinnerAdapter extends BaseAdapter {
    private final Context context;
    private final String[] standard;
    private final LayoutInflater inflter;


    public SpinnerAdapter(Context applicationContext, String[] standard) {
        this.context = applicationContext;
        this.standard = standard;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return standard.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        //ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        //icon.setImageResource(flags[i]);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(standard[i]);
        return view;
    }
}
