package com.findeed.Bot.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.findeed.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<SchoolNames> schoolList = null;
    private ArrayList<SchoolNames> arraylist;
    private String schoolname;


    public ListViewAdapter(Context context, ArrayList<SchoolNames> schoolList) {
        mContext = context;
        this.schoolList = schoolList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<SchoolNames>();
        this.arraylist.addAll(schoolList);
    }

    public class ViewHolder {
        TextView address;
        TextView name;
    }

    public String getSchoolname(){
        return this.schoolname;
    }

    @Override
    public int getCount() {
        return schoolList.size();
    }

    @Override
    public SchoolNames getItem(int position) {
        return schoolList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);

            holder.name = (TextView) view.findViewById(R.id.name);
            holder.address = (TextView) view.findViewById(R.id.address);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(schoolList.get(position).getSchoolname());
        holder.address.setText(schoolList.get(position).getSchooladdrs());
        schoolname = schoolList.get(position).getSchoolname();

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        schoolList.clear();
        if (charText.length() == 0) {
            schoolList.addAll(arraylist);
        } else {
            for (SchoolNames wp : arraylist) {
                if (wp.getSchoolname().toLowerCase(Locale.getDefault()).contains(charText)) {
                    schoolList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }



}
