package com.findeed.Bot.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findeed.Bot.ViewHolder.BaseViewHolder;
import com.findeed.Bot.ViewHolder.ChatInputVH;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponseVH;
import com.findeed.Bot.ViewHolder.ImageResponseVH;
import com.findeed.R;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private ArrayList<ChatObject> chatObjects;

    public ChatAdapter(ArrayList<ChatObject> chatObjects) {
        this.chatObjects = chatObjects;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Create the ViewHolder based on the viewType
        final View itemView;


        if(viewType==ChatObject.INPUT_OBJECT){
            itemView = inflater.inflate(R.layout.chat_input_layout, parent, false);
            return new ChatInputVH(itemView);
        }else if(viewType==ChatObject.RESPONSE_OBJECT) {
            itemView = inflater.inflate(R.layout.chat_response_layout, parent, false);
            return new ChatResponseVH(itemView);
        }else {
            itemView=inflater.inflate(R.layout.image_chat_input,parent,false);
            return  new ImageResponseVH(itemView);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBindView(chatObjects.get(position));


    }

    @Override
    public int getItemViewType(int position) {

        return chatObjects.get(position).getType();
    }
    @Override
    public int getItemCount() {
        return chatObjects.size();
    }
}
