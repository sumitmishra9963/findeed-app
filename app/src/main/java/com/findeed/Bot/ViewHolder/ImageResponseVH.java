package com.findeed.Bot.ViewHolder;

import android.view.View;
import android.widget.ImageView;

import com.findeed.R;

public class ImageResponseVH extends BaseViewHolder {
    private ImageView imageView;

    public ImageResponseVH(View itemView) {
        super(itemView);
        this.imageView=(ImageView)itemView.findViewById(R.id.inputImage);
    }

    @Override
    public void onBindView(ChatObject object) {
        this.imageView.setImageBitmap(object.getImage());

    }
}
