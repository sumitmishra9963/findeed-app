package com.findeed.Bot.ViewHolder;

import android.graphics.Bitmap;
import android.media.Image;
import android.support.annotation.NonNull;

public abstract class ChatObject {

    public static final int INPUT_OBJECT = 0;
    public static final int RESPONSE_OBJECT = 1;
    public static final int IMAGE_OBJECT=2;



    private String text;
    Bitmap bitmap;

    @NonNull
    public String getText() {
        return text;
    }

    @NonNull Bitmap getImage(){return  bitmap;}

    public void setText(@NonNull String text) {
        this.text = text;
    }
    public void setImage(@NonNull Bitmap bitmap){this.bitmap=bitmap;}

    public abstract int getType();
}
