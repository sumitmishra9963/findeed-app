package com.findeed.Bot.ViewHolder;


import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;



public class ChatResponseVH extends BaseViewHolder  {

    private TextView tvResponseText;
    private  LinearLayout bubbleLayout;
    LinearLayout responseView;

    public ChatResponseVH(View itemView) {
        super(itemView);
        this.tvResponseText = (TextView) itemView.findViewById(R.id.tv_response_text);
        bubbleLayout=(LinearLayout) itemView.findViewById(R.id.bubble);
        responseView=(LinearLayout)itemView.findViewById(R.id.msg);
    }


    @Override
    public void onBindView(final ChatObject object) {

        bubbleLayout.postDelayed(new Runnable() {
            public void run() {
                bubbleLayout.setVisibility(View.GONE);
                responseView.setVisibility(View.VISIBLE);


                if(ChatActivity.viewList.containsKey("Hello,Good Morning What type of loan are you looking out for?")){
                    try{
                        ChatActivity.viewList.get("Hello,Good Morning What type of loan are you looking out for?").setVisibility(View.VISIBLE);
                        ChatActivity.viewList.get("Which school does your child goes to ?").setVisibility(View.GONE);
                        ChatActivity.viewList.get("How many of your children study in ").setVisibility(View.GONE);
                        ChatActivity.viewList.get("How much loan do you require?").setVisibility(View.GONE);
                        ChatActivity.viewList.get("So what you do?").setVisibility(View.GONE);
                        ChatActivity.viewList.remove("Hello,Good Morning What type of loan are you looking out for?");
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }

                }

              else if(ChatActivity.viewList.containsKey("Which school does your child goes to ?"))
              {

                  try{
                      ChatActivity.viewList.get("Which school does your child goes to ?").setVisibility(View.VISIBLE);
                      ChatActivity.viewList.get("How many of your children study in ").setVisibility(View.GONE);
                      ChatActivity.viewList.get("How much loan do you require?").setVisibility(View.GONE);
                      ChatActivity.viewList.get("So what you do?").setVisibility(View.GONE);
                      ChatActivity.viewList.remove("Which school does your child goes to ?");


                  }catch (NullPointerException e){
                      e.printStackTrace();
                  }


             }
               else if(object.getText().equals("How much loan do you require?")) {
                   try{
                       ChatActivity.viewList.get("How much loan do you require?").setVisibility(View.VISIBLE);
                       ChatActivity.viewList.get("So what you do?").setVisibility(View.VISIBLE);
                       ChatActivity.viewList.get("So what you do?").setVisibility(View.GONE);
                       ChatActivity.viewList.remove("How much loan do you require?");
                   }catch (NullPointerException e){
                       e.printStackTrace();
                   }

                }
             else   if(object.getText().equals("So what you do?")){
                 try{

                     ChatActivity.viewList.get("So what you do?").setVisibility(View.VISIBLE);
                     ChatActivity.viewList.remove("So what you do?");

                 }catch (NullPointerException e){
                     e.printStackTrace();
                 }

                }else if(object.getText().contains("How many of your children study in ")) {
                    try{
                        ChatActivity.viewList.get("How many of your children study in ").setVisibility(View.VISIBLE);
                        ChatActivity.viewList.get("How much loan do you require?").setVisibility(View.GONE);
                        ChatActivity.viewList.get("So what you do?").setVisibility(View.GONE);
                        ChatActivity.viewList.remove("Which school does your child goes to ?");
                        ChatActivity.viewList.remove("How many of your children study in ");

                    }catch (NullPointerException e){

                        e.printStackTrace();

                    }
                }
             else if(object.getText().equals("Can you provide us your PAN card for ID verification?")){
                 try{
                     ChatActivity.viewList.get("Can you provide us your PAN card for ID verification?").setVisibility(View.VISIBLE);
                     ChatActivity.viewList.remove("Can you provide us your PAN card for ID verification?");


                 }catch (NullPointerException e){
                     e.printStackTrace();
                 }
                }


            }

        }, 1000);

        this.tvResponseText.setText(object.getText());

    }
}
