package com.findeed.Bot;

import android.graphics.Bitmap;

import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponse;
import com.findeed.Bot.ViewHolder.ImageResponse;

import java.util.ArrayList;

public class ChatPresenter implements ChatContract.Presenter {

    private static ArrayList<ChatObject> chatObjects;
    private  ChatContract.View view;

    public ChatPresenter() {
        // Create the ArrayList for the chat objects
        this.chatObjects = new ArrayList<>();

    }

    @Override
    public void attachView(ChatContract.View view) {
        this.view = view;
    }

    @Override
    public ArrayList<ChatObject> getChatObjects() {
        return this.chatObjects;
    }

    @Override
    public void onEditTextActionDone(String inputText) {
        // Create new input object
        ChatInput inputObject = new ChatInput();
        inputObject.setText(inputText);

        // Add it to the list and tell the adapter we added something
        this.chatObjects.add(inputObject);
        view.notifyAdapterObjectAdded(chatObjects.size() - 1);

        // Also scroll down if we aren't at the bottom already
        view.scrollChatDown();
    }

    @Override
    public void botMessage(String inputText) {
        ChatResponse botReply =new ChatResponse();
        botReply.setText(inputText);
        chatObjects.add(botReply);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();
    }


    public static void setImage(Bitmap bitmap){
        ImageResponse imageResponse = new ImageResponse();
        imageResponse.setImage(bitmap);
        chatObjects.add(imageResponse);

    }
}
