package com.findeed;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.findeed.Bot.ChatActivity;
import com.findeed.Otp.OtpResponse;
import com.google.gson.Gson;

import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PhoneRegisterActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    EditText phoneNumber;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_register);

        linearLayout = findViewById(R.id.llnextbutton);
        phoneNumber=findViewById(R.id.phoneNumber);

        phoneNumber.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (phoneNumber.getText().length() != 10) {
                    phoneNumber.setError("Invalid phone number");

                } else {
                    phoneNumber.setError(null);

                }
            }
        });


        linearLayout.setOnClickListener(v -> {
          if(phoneNumber.getText().toString().length()!=10)  {
              Toast.makeText(this,"Invalid phone number",Toast.LENGTH_LONG).show();
          }
          else{

              new getOtp().execute();


          }

      });


    }


    public class getOtp extends AsyncTask<String,String,String>{
        ProgressDialog pd = new ProgressDialog(PhoneRegisterActivity.this);

        @Override
        protected void onPreExecute() {
            pd.setMessage("Sending OTP...");
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            if (pd.isShowing()) {
                pd.dismiss();
                Intent intent = new Intent(PhoneRegisterActivity.this, VerifyOTPActivity.class);
                intent.putExtra("phone",phoneNumber.getText().toString());
                startActivity(intent);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            RequestBody reqbody = RequestBody.create(null, new byte[0]);
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los/otp/generateOTP/"+phoneNumber.getText().toString())
                    .post(reqbody)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Gson gson = new Gson();
                OtpResponse res = gson.fromJson(resData,OtpResponse.class);
                Log.d("####################OTP",res.getData());
                return resData;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
}
